<div class="container-fluid">

    <div class="row">

        <div class="col-lg-12">

            <ul class="list-inline">

                <li id="show-hide-button">
                <a href="#menu-toggle" id="menu-toggle">
                    <button class="tog btn btn-success" onclick="saveCan(canvas); changeView(2); loadCan(canvas2);">Hide</button>
                    <button class="show-hide tog btn btn-success" onclick="saveCan(canvas2); changeView(1); loadCan(canvas);">Show</button>
                </a>
                </li> <!-- show-hide-button END -->
                <li>
                    <div class="dropdown">
                        <button class="btn btn-success dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Items
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu list-inline" role="menu" aria-labelledby="menu1">
                            <li id="b1" role="presentation"><a role="menuitem" tabindex="-1" href="#">Rectangle</a></li>
                            <li id="b2" role="presentation"><a role="menuitem" tabindex="-1" href="#">Circle</a></li>
                            <li id="b3" role="presentation"><a role="menuitem" tabindex="-1" href="#">Join</a></li>
                            <li id="b4" role="presentation"><a role="menuitem" tabindex="-1" href="#">Text</a></li>
                            <li id="b6" role="presentation"><a role="menuitem" tabindex="-1" href="#">Clear</a></li>
                            <li id="imgLoader" role="presentation"><a role="menuitem" tabindex="-1" href="#"><input type="file" id="imgLoader" onclick='loadImage();'></a></li>
                            <li id="b7" role="presentation"><a role="menuitem" tabindex="-1" href="#">Enter Drawing Mode</a></li>
                            <li id="b8" role="presentation"><a role="menuitem" tabindex="-1" href="#">Exit Drawing Mode</a></li>
                            <li id="b9" role="presentation"><a role="menuitem" tabindex="-1" href="#">Delete</a></li>
                        </ul>
                    </div> <!-- dropdown END -->
                </li>
                <li><button class="btn btn-success" onclick="newCanvas();">New</button></li>
                <li id="projectsList" class="list-inline">
                    {{ partial("shared/projectList") }}
                </li>

            </ul> <!-- list-inline END -->

            <div id="toggle">
                <canvas id="canvas" width="1047" height="531"></canvas>
                <canvas id="canvas2" width="1297" height="531"></canvas>
                {{ javascript_include("js/brainboard-fabric.js") }}

                {{ content() }}
            </div> <!-- toggle END -->

        </div> <!-- col-lg-12 END -->

    </div> <!-- row END -->

</div> <!-- container-fluid END -->


    