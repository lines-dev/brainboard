<!DOCTYPE html>
<html>
	<head>
                <link rel="shortcut icon" href="{{ url("img/favicon.ico") }}" type="image/x-icon">
                <link rel="icon" href="{{ url("img/favicon.ico") }}" type="image/x-icon">

                <title>BrainBoard</title>
                {{ stylesheet_link("css/bootstrap/bootstrap2.min.css") }}
                {{ stylesheet_link("css/bootstrap/bootstrap-theme.min.css") }}
                {{ stylesheet_link("css/bootstrap/bootstrap_and_overides.css.less") }}
                {{ stylesheet_link("css/bootstrap/simple-sidebar.css") }}
                {{ javascript_include("js/jquery/jquery-1.11.2.min.js") }}
                {{ javascript_include("js/bootstrap/bootstrap2.min.js") }}
                {{ javascript_include("js/bootbox.min.js") }}
                
                {{ javascript_include("js/fabric/fabric.min.js") }}
                {{ javascript_include("js/brainboard-fabric.js") }}
                
	</head>
	<body>
            <div id="fb-root"></div>

        <div id="wrapper">

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                {{ partial("shared/navbar") }}
            </div>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">
                {{ content() }}
            </div>
            <!-- /#page-content-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Menu Toggle Script -->
        <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
            $( ".tog" ).toggle();
        });
        </script>

	</body>
</html>