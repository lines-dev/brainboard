    <ul class="sidebar-nav">

        <li class="sidebar-brand"><a href="#"><img src="public/img/logo.png" class="img-responsive ourlogo" alt="Image"></a></li>

        {{ javascript_include("js/fbApi.js") }}

        <div class="fb">
            <div class="fb-login-button"
                style="float:right;"
                id="fbLoginButton"
                data-scope="public_profile,email"
                data-onlogin="checkLoginState();"
                data-max-rows="1"
                data-size="large"
                data-show-faces="false"
                data-auto-logout-link="true">
            </div>
        </div>
    </ul>
