{% if isLogged %}
    
<script type="text/javascript">
$(document).ready(function(){
    
     currentBoardId = {{ curBID }};
     $("#naziv").val("{{ curBNaziv }}");

    $('#save-button').click( function() {
        if($('#canvas:visible').length == 0) content = JSON.stringify(canvas2);
        else content = JSON.stringify(canvas);
        //alert("test");
        if(BbUser){
            currentBoardId = (typeof currentBoardId != 'undefined')?currentBoardId:0;
            //var name = window.prompt("Name your canvas", currentBoardName);
            $.post(
                "/brainboard/Board/saveBoard",
                {
                    uid : BbUser.ID,
                    bid : currentBoardId,
                    naziv : $("#naziv").val(),
                    content : content
                },
                function(d){
                    //alert(newBid);
                    //location.reload();
                    //alert(d);
                    d = JSON.parse(d);
                    //alert(d.ID);
                    currentBoardId = d.ID;
                    
                    bootbox.alert(d.poruka);
                    if(currentBoardId == 0) return;
                    //refreshProjectList(currentBoardId);
                    $("[projektID='"+d.ID+"']").remove();
                    $("#rawlista").append('<li class="projekt-open" projektID="'+ d.ID +'" role="presentation"><a role="menuitem" tabindex="-1" href="#">'+ d.boardNaziv +'</a></li>');
                    $("#projekt-dropdown").fadeIn();
                }
            );
        }
        
    });
    
    if($("#rawlista").children().length == 0){
        $("#projekt-dropdown").hide();
    }
    
    $(".projekt-open").click(function(){
        cbid = $(this).attr("projektID");

        $.post("/brainboard/Board/getBoardByID", { bid: cbid }, function(d){
            if(d!=0){
                d = JSON.parse(d);
                currentBoardId = d.bID;
                $("#naziv").val(d.naziv);
                loadCan2(d.content);
            }
        });

    });

});


</script>
    
    <li><input class="form-control" type="text" id="naziv" value="" /></li>
    <li><button class="btn btn-info" id="save-button">Save</button></li>
    <li>
            <div id ="projekt-dropdown" class="dropdown projects-menu">
                <button class="btn btn-warning dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Projects
                <span class="caret"></span></button>
                <ul id="rawlista" class="dropdown-menu" role="menu" aria-labelledby="menu1">
                {% if boards is defined %}
                {% for board in boards %}
                    <li class="projekt-open" projektID="{{ board.bID }}" role="presentation"><a role="menuitem" tabindex="-1" href="#">{{board.naziv}}</a></li>
                {% endfor %}
                {% endif %}
                </ul>
            </div>
    </li>
{% endif %}