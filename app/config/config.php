<?php

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'brainboard',
        'password'    => 'm1k1gay',
        'dbname'      => 'brainboard',
    ),
    'application' => array(
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'modelsDir'      => __DIR__ . '/../../app/models/',
        'viewsDir'       => __DIR__ . '/../../app/views/',
        'pluginsDir'     => __DIR__ . '/../../app/plugins/',
        'libraryDir'     => __DIR__ . '/../../app/library/',
        'facebookDir'    => __DIR__ . '/../../app/vendor/facebook/php-sdk-v4/src/Facebook/',
        'cacheDir'       => __DIR__ . '/../../app/cache/',
        'baseUri'        => '/brainboard/',
    ),
    'facebook' => array(
        'appId'     =>  '382793118547550',
        'secret'    =>  '2855d1e74a1e46d2ae97e4bde74f924d'
    )
));