<?php

class Board extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $bID;

    /**
     *
     * @var integer
     */
    public $uID;

    /**
     *
     * @var string
     */
    public $naziv;

    /**
     *
     * @var string
     */
    public $content;

}
