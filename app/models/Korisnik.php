<?php

use Phalcon\Mvc\Model\Validator\Email as Email;

class Korisnik extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $ID;

    /**
     *
     * @var integer
     */
    public $FBID;

    /**
     *
     * @var string
     */
    public $ime;

    /**
     *
     * @var string
     */
    public $prezime;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $spol;

    /**
     * Validations and business logic
     */
    public function validation()
    {

        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }
    
    public function updateDataFromFb($FbUser){
        $this->FBID     = $FbUser->getId();
        $this->ime      = $FbUser->getFirstName();
        $this->prezime  = $FbUser->getLastName();
        $this->email    = $FbUser->getEmail();
        $this->spol     = $FbUser->getGender();
        $this->save();
    }
    
}