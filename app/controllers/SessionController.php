<?php

use Facebook\FacebookRequest as FacebookRequest;
use Facebook\FacebookRequestException as FacebookRequestException;
use Facebook\GraphUser as GraphUser;

class SessionController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {
        $this->view->disable();
    }
    
    public function createSessionAction(){
        $this->view->disable();

        if ($this->FBSession) {
            try{
                $FbUser = (new FacebookRequest(
                    $this->FBSession, 'GET', '/me'
                ))->execute()->getGraphObject(GraphUser::className());
            }catch(FacebookRequestException $e) {
                echo "Exception occured, code: " . $e->getCode();
                echo " with message: " . $e->getMessage();
            }
            
            if($FbUser){
                
                $BbUser = KorisnikController::checkFbUser($FbUser);
                
                $this->session->set("user", array(
                    "ID" => $BbUser->ID,
                    "FBID" => $BbUser->FBID,
                    "email" => $BbUser->email
                ));
                
                echo json_encode($BbUser);
            }
        }
        
    }
    
    public function logoutAction(){
        self::destroySession();
    }
    
    public static function destroySession(){
        self::unsetUser();
        $this->session->destroy();
    }
    
    public static function unsetUser(){
        $this->session->remove("user");

    }
    
    public static function checkSession(){
        
        if(self::userLogged() && !$this->FBSession){
            self::unsetUser();
        }
        
    }
    
    public static function userLogged(){
        return $this->session->has("user");
    }

}

