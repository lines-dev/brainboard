<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    
    public function initialize(){
        
        
        if(!$this->FBSession && $this->session->has("user")){
            $this->session->remove("user");
        }
        
        echo $this->session->get("user")["ID"];
        
        $this->view->isLogged = $this->session->has("user");
       
        //echo $this->session->get("user")["ID"];
        
        if($this->session->has("user")){
            $boards = BoardController::getUserBoards($this->session->get("user"));
            $this->view->boards = $boards;
        }else{
            $this->view->boards = null;
        }
              
    }
    
}
