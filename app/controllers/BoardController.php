<?php

class BoardController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {
        
    }
    
    public function saveBoardAction(){
        
        $this->view->disable();
        //$this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        
        $uid = $this->request->getPost("uid");
        $bid = $this->request->getPost("bid");
        $naziv = $this->request->getPost("naziv");
        $content = $this->request->getPost("content");
        
        if($bid==0){
            $board = new Board();
            $board->uID = $uid;
            $board->naziv = $naziv;
            $board->content = $content;
        }else{
            $board = Board::findFirst("bID = " . $bid);
            $board->naziv = $naziv;
            $board->content = $content;
        }
        
        $return = array();
        
        if($board->save()){
            $return["ID"] = $board->bID;
            $return["boardNaziv"] = $board->naziv;
            $return["poruka"] = "Board successfully saved.";
             
        } else {
            $return["ID"] = 0;
            $return["poruka"] = "Board not saved!";
            //foreach ($board->getMessages() as $m){
            //    echo $m. " ";
            //}
        }
        
        echo json_encode($return);
        
    }
    
    public function getProjectsAction(){
        
        $this->view->disable();
        //$this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
        
        $boards = self::getUserBoards($this->session->get("user"));
        
        $curBID = $this->request->get("curID");
        if($curBID != 0){
            $this->view->curBID = $curBID;
            $currentBoard = self::getBoard($curBID);
            $this->view->curBNaziv = $currentBoard.naziv;
        }else{
            $this->view->curBID = 0;
            $this->view->curBNaziv = "New Canvas";
        }
        if($boards->getFirst())
            $this->view->boards = $boards;
        else
            $this->view->boards = null;
        $this->view->isLogged = $this->session->has("user");
        echo $this->view->partial("shared/projectList", $boards);
        
    }
    
    public function getBoardByIDAction(){
        
        //$this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $this->view->disable();
        

        if($this->session->has("user")){
            $bid = $this->request->get("bid");
            $board = self::getBoard($bid);
            echo json_encode($board);
        }else{
            echo "0";
        }
    }
    
    public static function getUserBoards($uid){
        return Board::find(array("uID" => $uid));
    }
        
    private static function getBoard($bid){
        return Board::findFirst("bID = " . $bid);
    }
    
}

