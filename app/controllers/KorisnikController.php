<?php

class KorisnikController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }
    
    private static function findBbUserByFbUser($FbUser){
        return Korisnik::findFirst("FBID = " . $FbUser->getId());
    }
    
    private static function createNewBbUserWithFbUser($FbUser){
        $newBbUser = new Korisnik();
        $newBbUser->updateDataFromFb($FbUser);
        $newBbUser->refresh();
        return $newBbUser;
    }
    
    public static function checkFbUser($FbUser){
        
        $BbUser = self::findBbUserByFbUser($FbUser);
        
        if($BbUser){
            $BbUser->updateDataFromFb($FbUser);
            return $BbUser;
        }else{
            return self::createNewBbUserWithFbUser($FbUser);
        }
        
    }
    
}