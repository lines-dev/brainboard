var activeCanvas, canvas, canvas2;

canvas = new fabric.Canvas('canvas');
canvas.backgroundColor = 'rgb(214,231,212)';
canvas2 = new fabric.Canvas('canvas2');
canvas2.backgroundColor = 'rgb(214,231,0)';
changeView(1);
/*
function resize(){
	canvas.setWidth($( window ).width() - 350);
	canvas.setHeight( $( window ).height() );
	canvas.calcOffset();

	canvas2.setWidth($( window ).width() - 350);
	canvas2.setHeight( $( window ).height() );
	canvas2.calcOffset();
}

window.addEventListener('resize', resize, false);*/

function refreshProjectList(currentBoardId){
    currentBoardId = typeof currentBoardId !== 'undefined' ? currentBoardId : 0;
    //alert(currentBoardId);
    $.post("/brainboard/Board/getProjects", {curID:currentBoardId}, function(d){
        $("#projectsList").html(d);
    });
}

function changeView(value) {
	if (value == 1) {
		activeCanvas = canvas;

		$('#canvas').parent().css('display', 'block');

		$('#canvas2').parent().css('display', 'none');
	}

	if (value == 2) {
		activeCanvas = canvas2;

		$('#canvas').parent().css('display', 'none');

		$('#canvas2').parent().css('display', 'block');
	}
}

function saveCan(value) {
	save = JSON.stringify(value);
}
function loadCan(value) {
	value.loadFromJSON(save);
}
function loadCan2(string) {
	canvas.loadFromJSON(string);
	canvas.renderAll();
        canvas2.loadFromJSON(string);
	canvas2.renderAll();
}/*
function loadCanvas(c, id, naziv){
    currentBoardId=id;
    $("#naziv").val(naziv);
    loadCan2(c);
}
 */

function newCanvas(){
    
    canvas.clear();
    canvas2.clear();
    currentBoardId = 0;
    $("#naziv").val("New Canvas");
    
}

$(document).ready(function(){
    newCanvas();
});

canvas.renderAll();

canvas.observe("object:moving", function(e){
	var obj = e.target;

	var halfw = obj.currentWidth/2;
	var halfh = obj.currentHeight/2;
	var bounds = {tl: {x: halfw, y:halfh},
		br: {x: obj.canvas.width-50, y: obj.canvas.height-50 }
	};

	// top-left  corner
	if(obj.top < bounds.tl.y || obj.left < bounds.tl.x){
		obj.top = Math.max(obj.top, '0'  );
		obj.left = Math.max(obj.left , '0' );
	}

	// bot-right corner
	if(obj.top > bounds.br.y || obj.left > bounds.br.x ){
		obj.top = Math.min(obj.top, '0');
		obj.left = Math.min(obj.left, '0' );
	}
});

function loadImage() {
document.getElementById('imgLoader').onchange = function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        var imgObj = new Image();
        imgObj.src = event.target.result;
        imgObj.onload = function () {
            var image = new fabric.Image(imgObj);
            image.set({
                top: canvas.height/2-100,
				left: canvas.width/2-100,
				scaleX:.25,
				scaleY:.25
            });
			if($('#canvas:visible').length == 0) canvas2.add(image);
			else canvas.add(image);
        }
    }
    reader.readAsDataURL(e.target.files[0]);
	$('#imgLoader').val('');
}
}

$('#b1').click( function() {
	var rect = new fabric.Rect({
		width: 200,
		height: 100,
		top: canvas.height/2-100,
		left: canvas.width/2-100,
		fill: '#52B2CB',
		rx: 10,
		ry: 10
	});
	if($('#canvas:visible').length == 0) canvas2.add(rect);
	else canvas.add(rect);
});

$('#b2').click( function() {
	var circle = new fabric.Circle({
		radius: 60,
		top: canvas.height/2-40,
		left: canvas.width/2-40,
		fill: '#CB8952'
	});
	if($('#canvas:visible').length == 0) canvas2.add(circle);
	else canvas.add(circle);
});

$('#b3').click( function() {

(function() {

	if($('#canvas:visible').length == 0)
	{
		canvas2.on({
			'object:selected': onObjectSelected,
			'object:moving': onObjectMoving,
			'before:selection:cleared': onBeforeSelectionCleared
		});
	}
	else
	{
		canvas.on({
			'object:selected': onObjectSelected,
			'object:moving': onObjectMoving,
			'before:selection:cleared': onBeforeSelectionCleared
		});
	}
	
	(function drawQuadratic() {
	
		var line = new fabric.Path('M 65 0 Q 100, 100, 200, 0', { fill: '', stroke: 'black' });
	
		line.path[0][1] = 100;
		line.path[0][2] = 100;
		
		line.path[1][1] = 200;
		line.path[1][2] = 200;
		
		line.path[1][3] = 300;
		line.path[1][4] = 100;
		
		line.selectable = true;
		if($('#canvas:visible').length == 0) canvas2.add(line);
		else canvas.add(line);
	
		var p1 = makeCurvePoint(200, 200, null, line, null)
		p1.name = "p1";
		if($('#canvas:visible').length == 0) canvas2.add(p1);
		else canvas.add(p1);
		
		var p0 = makeCurveCircle(90, 90, line, p1, null);
		p0.name = "p0";
		if($('#canvas:visible').length == 0) canvas2.add(p0);
		else canvas.add(p0);
		
		var p2 = makeCurveCircle(290, 90, null, p1, line);
		p2.name = "p2";
		if($('#canvas:visible').length == 0) canvas2.add(p2);
		else canvas.add(p2);
	})();
	
	function makeCurveCircle(left, top, line1, line2, line3) {
		var c = new fabric.Circle({
			left: left,
			top: top,
			strokeWidth: 5,
			radius: 12,
			fill: '#fff',
			stroke: '#666'
		});
	
		c.hasBorders = c.hasControls = false;
		
		c.line1 = line1;
		c.line2 = line2;
		c.line3 = line3;
		
		return c;
	}
	
	function makeCurvePoint(left, top, line1, line2, line3) {
		var c = new fabric.Circle({
			left: left,
			top: top,
			strokeWidth: 8,
			radius: 14,
			fill: '#fff',
			stroke: '#666'
		});
	
		c.hasBorders = c.hasControls = false;
		
		c.line1 = line1;
		c.line2 = line2;
		c.line3 = line3;
		
		return c;
	}
	
	function onObjectSelected(e) {
		var activeObject = e.target;
	
		if (activeObject.name == "p0" || activeObject.name == "p2") {

			if($('#canvas:visible').length == 0){
				activeObject.line2.animate('opacity', '1', {
					duration: 200,
					onChange: canvas2.renderAll.bind(canvas2),
				});
			}
			else{
				activeObject.line2.animate('opacity', '1', {
					duration: 200,
					onChange: canvas.renderAll.bind(canvas),
				});
			}

			activeObject.line2.selectable = true;
		}
	}
	
	function onBeforeSelectionCleared(e) {
		var activeObject = e.target;
		if (activeObject.name == "p0" || activeObject.name == "p2") {

			if($('#canvas:visible').length == 0){
				activeObject.line2.animate('opacity', '0', {
				duration: 200,
				onChange: canvas.renderAll.bind(canvas2),
			}); }
			else{
				activeObject.line2.animate('opacity', '0', {
				duration: 200,
				onChange: canvas.renderAll.bind(canvas),
			}); }

	  		activeObject.line2.selectable = false;
		}
		else if (activeObject.name == "p1") {

			if($('#canvas:visible').length == 0){
				activeObject.animate('opacity', '0', {
					duration: 200,
					onChange: canvas2.renderAll.bind(canvas),
				});
			}
			else{
				activeObject.animate('opacity', '0', {
					duration: 200,
					onChange: canvas.renderAll.bind(canvas),
				});
			}

	  		activeObject.selectable = false;
		}
	}
	
	function onObjectMoving(e) {
		if (e.target.name == "p0" || e.target.name == "p2") {
	 		var p = e.target;
	
		  	if (p.line1) {
				p.line1.path[0][1] = p.left;
				p.line1.path[0][2] = p.top;
		  	}
	  		else if (p.line3) {
				p.line3.path[1][3] = p.left;
				p.line3.path[1][4] = p.top;
	  		}
		}
		else if (e.target.name == "p1") {
			var p = e.target;
		
		    if (p.line2) {
				p.line2.path[1][1] = p.left;
				p.line2.path[1][2] = p.top;
		  	}
		}
		else if (e.target.name == "p0" || e.target.name == "p2") {
	 		var p = e.target;
	
			p.line1 && p.line1.set({ 'x2': p.left, 'y2': p.top });
			p.line2 && p.line2.set({ 'x1': p.left, 'y1': p.top });
			p.line3 && p.line3.set({ 'x1': p.left, 'y1': p.top });
			p.line4 && p.line4.set({ 'x1': p.left, 'y1': p.top });
		}
	}
})();

});

$('#b4').click( function() {
	var text = new fabric.IText('Tap and Type', { 
		fontFamily: 'verdana black',
		top: canvas.height/2-100,
		left: canvas.width/2-100,
		fontSize: 16
	});
	if($('#canvas:visible').length == 0) canvas2.add(text);
	else canvas.add(text);
});


$('#b6').click( function() {
    if (confirm("Do you want to clear canvas?") == true) {
		if($('#canvas:visible').length == 0) canvas2.clear();
		else canvas.clear();
    }
});
var save;
$('#b7').click( function() {
	if($('#canvas:visible').length == 0) canvas2.isDrawingMode = true;
	else canvas.isDrawingMode = true;
});

$('#b8').click( function() {
	if($('#canvas:visible').length == 0) canvas2.isDrawingMode = false;
	else canvas.isDrawingMode = false;
});

$("#b9").click(function(){
	if($('#canvas:visible').length == 0)
	{
		canvas2.isDrawingMode = false;

		var activeObject = canvas2.getActiveObject(), activeGroup = canvas2.getActiveGroup();

		if(activeObject){ canvas2.remove(activeObject); }
		else if(activeGroup){
			var objectsInGroup = activeGroup.getObjects();
			canvas2.discardActiveGroup();
			objectsInGroup.forEach(function(object) {
				canvas2.remove(object);
			});
		}
	}
	else
	{
		canvas.isDrawingMode = false;

		var activeObject = canvas.getActiveObject(), activeGroup = canvas.getActiveGroup();

		if(activeObject){ canvas.remove(activeObject); }
		else if(activeGroup){
			var objectsInGroup = activeGroup.getObjects();
			canvas.discardActiveGroup();
			objectsInGroup.forEach(function(object) {
				canvas.remove(object);
			});
		}
	}
});