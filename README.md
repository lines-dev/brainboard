# Brainboard

Brainboard je projekt u sklopu kolegija "Elektroničko gospodarstvo" na diplomskom studiju Odjela za Informatiku Sveučilišta u Rijeci.

Autori:

  * Kristijan Lenković, *univ. bacc. inf.*
  * Mislav Miočević, *univ. bacc. inf.*
  * Edvin Močibob, *univ. bacc. inf.*

## Changelog
0.1.0 - Inicijalna verzija projekta

## Requirements
 * PHP >= 5.4
 * MySQL >= 5.5
 * Phalcon Framework >= 1.3.2

## Development

Glavni dev repozitorij dostupan je preko BitBucket servisa.
```sh
$ cd /var/www/
$ git clone https://[username]@bitbucket.org/cloroformus/brainboard.git
```

Nakon povlačenja repozitorija, potrebno je pomoću phpMyAdmina kreirati
novog korisnika s podacima:

Username: **brainboard**
Password: **m1k1gay**

Potrebno je označiti opciju:
"**Create database with same name and grant all privileges**"

Nakon što je korisnik **brainboard** i baza **brainboard** uspješno kreirana,
može se izvršiti import SQL datoteke **brainboardDB-struktura.sql** pomoću
phpMyAdmina koja se nalazi u rootu repozitorija.

### Facebook integracija i funkcionalnosti

Kako bi aplikacija bila u potpunosti funkcionalna, potrebno je pripremiti
lokalno računalo za bypass Facebook security-a.

**Linux:**
```sh
$ sudo nano /etc/hosts
```
Dodati liniju u host file:
```
127.0.1.1       cloroformus.dev
```
Zatim je potrebno lokalno pristupati aplikaciji pomoću URLa:
```
http://cloroformus.dev
```

**Windows:**
Upute dostupne [ovdje](http://support.hostgator.com/articles/general-help/technical/how-do-i-change-my-hosts-file).
Podaci za konfiguraciju bi trebali biti isti kao i za Linux.

Kada se aplikacija prebaci na pravi server, ovo će se vjerojatno izmjeniti.
Facebook je na tjednoj bazi podložan promjenama i moguće je da se ovaj bypass ukine.

## Todo's

Dostupno na online servisu [Trello].

[Trello]:https://trello.com/b/04kOEoQh/brainboard