-- STRUKTURA BRAINBOARD BAZE

-- Ukoliko se podaci importiraju po prvi put, prije importa
-- pomoću phpMyadmina potrebno je kreirati
-- novog korisnika s podacima:
--
-- Username: brainboard
-- Password: m1k1gay
--
-- Potrebno je označiti opciju:
-- "Create database with same name and grant all privileges"



-- --------------------------------------------------------

-- Host: localhost
-- Generation Time: Jan 11, 2015 at 04:09 AM
-- Server version: 5.5.40
-- PHP Version: 5.4.36-0+deb7u1

-- --------------------------------------------------------

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `brainboard`
--

USE `brainboard`;

-- --------------------------------------------------------

--
-- Brisanje starih i novih struktura prije importa
--

DROP TABLE IF EXISTS `Korisnik`;
DROP TABLE IF EXISTS `korisnik`;
DROP TABLE IF EXISTS `Board`;
DROP TABLE IF EXISTS `board`;

-- --------------------------------------------------------

--
-- Table structure for table `board`
--

CREATE TABLE IF NOT EXISTS `board` (
  `bID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uID` bigint(20) unsigned NOT NULL,
  `naziv` varchar(40) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`bID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE IF NOT EXISTS `korisnik` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `FBID` bigint(20) unsigned NOT NULL,
  `ime` varchar(40) NOT NULL,
  `prezime` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `spol` varchar(6) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `FBID` (`FBID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;